// ignore_for_file: public_member_api_docs
@JS()
library firebase_node.admin.interop.project_management;

import 'package:js/js.dart';

import '../../utils/es6_interop.dart';

@JS('AndroidApp')
@anonymous
abstract class AndroidAppJsImpl {
  external String get appId;
  external PromiseJsImpl<void> addShaCertificate(ShaCertificate certificate);
  external PromiseJsImpl<void> deleteShaCertificate(ShaCertificate certificate);
  external PromiseJsImpl<String> getConfig();
  external PromiseJsImpl<IosAppMetadata> getMetadata();
  external PromiseJsImpl<ShaCertificate> getShaCertificates();
  external PromiseJsImpl<void> setDisplayName(String newDisplayName);
}

@JS()
@anonymous
abstract class AndroidAppMetadata extends AppMetadata {
  String get packageName;
}

@JS()
@anonymous
abstract class AppMetadata {
  external String get appId;
  external String get displayName;
  external String get platform;
  external String get projectId;
  external String get resourceName;
}

@JS('IosApp')
@anonymous
abstract class IosAppJsImpl {
  external String get appId;
  external PromiseJsImpl<String> getConfig();
  external PromiseJsImpl<IosAppMetadata> getMetadata();
  external PromiseJsImpl<void> setDisplayName(String newDisplayName);
}

@JS()
@anonymous
abstract class IosAppMetadata extends AppMetadata {
  String get bundleId;
}

@JS('ProjectManagement')
@anonymous
abstract class ProjectManagementJsImpl {
  external AndroidAppJsImpl androidApp(String appId);
  external PromiseJsImpl<AndroidAppJsImpl> createAndroidApp(
      String packageName, String displayName);
  external PromiseJsImpl<IosAppJsImpl> createIosApp(
      String bundleId, String displayName);
  external ShaCertificate shaCertificate(String shaHash);
  external IosAppJsImpl iosApp(String appId);
  external PromiseJsImpl<List> listAndroidApps();
  external PromiseJsImpl<List> listAppMetadata();
  external PromiseJsImpl<List> listIosApps();
  external PromiseJsImpl<void> setDisplayName(String newDisplayName);
}

/// A SHA-1 or SHA-256 certificate.
///
/// Do not call this constructor directly. Instead, use projectManagement.
/// shaCertificate().
@JS()
@anonymous
abstract class ShaCertificate {
  /// A SHA-1 or SHA-256 certificate.
  ///
  /// Do not call this constructor directly. Instead, use projectManagement.
  /// shaCertificate().
  external factory ShaCertificate({
    String certType,
    String resourceName,
    String shaHash,
  });
  external String get certType;
  external set certType(String v);
  external String get resourceName;
  external set resourceName(String v);
  external String get shaHash;
  external set shaHash(String v);
}
