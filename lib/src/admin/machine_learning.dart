part of '../../admin.dart';

///Response object for a listModels operation.
class ListModelsResult
    extends JsObjectWrapper<ml_interop.ListModelsResultJsImpl>
    implements _ListModelsResult {
  static final _expando = Expando<ListModelsResult>();

  ListModelsResult._fromJsObject(ml_interop.ListModelsResultJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  List<Model> get models => List.from(jsObject.models);

  @override
  String get pageToken => jsObject.pageToken;

  ///Response object for a listModels operation.
  static ListModelsResult getInstance(
      ml_interop.ListModelsResultJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= ListModelsResult._fromJsObject(jsObject);
  }
}

/// The Firebase MachineLearning service interface.
class MachineLearning extends JsObjectWrapper<ml_interop.MachineLearningJsImpl>
    implements _MachineLearning {
  static final _expando = Expando<MachineLearning>();

  MachineLearning._fromJsObject(ml_interop.MachineLearningJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  App get app => App.getInstance(jsObject.app);

  @override
  Future<void> createModel(ModelOptions model) =>
      handleThenable(jsObject.createModel(model.jsObject));

  @override
  Future<void> deleteModel(String modelId) =>
      handleThenable(jsObject.deleteModel(modelId));

  @override
  Future<Model> getModel(String modelId) =>
      handleThenable(jsObject.getModel(modelId)).then(Model.getInstance);

  @override
  Future<ListModelsResult> listModels(ml_interop.ListModelsOptions options) =>
      handleThenable(jsObject.listModels(options))
          .then(ListModelsResult.getInstance);

  @override
  Future<Model> publishModel(String modelId) =>
      handleThenable(jsObject.publishModel(modelId)).then(Model.getInstance);
  @override
  Future<Model> unpublishModel(String modelId) =>
      handleThenable(jsObject.unpublishModel(modelId)).then(Model.getInstance);

  @override
  Future<Model> updateModel(String modelId, ModelOptions model) =>
      handleThenable(jsObject.updateModel(modelId, model.jsObject))
          .then(Model.getInstance);

  /// The Firebase MachineLearning service interface.
  ///
  /// Do not call this constructor directly. Instead, use
  /// admin.machineLearning().
  static MachineLearning getInstance(
      ml_interop.MachineLearningJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= MachineLearning._fromJsObject(jsObject);
  }
}

///A Firebase ML Model output object
class Model extends JsObjectWrapper<ml_interop.ModelJsImpl> implements _Model {
  static final _expando = Expando<Model>();

  Model._fromJsObject(ml_interop.ModelJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  String get createTime => jsObject.createTime;

  @override
  String get displayName => jsObject.displayName;

  @override
  String get etag => jsObject.etag;

  @override
  bool get locked => jsObject.locked;

  @override
  String get modelHash => jsObject.modelHash;

  @override
  String get modelId => jsObject.modelId;

  @override
  bool get published => jsObject.published;

  @override
  List<String> get tags => jsObject.tags;

  @override
  ml_interop.TFLiteModel get tfliteModel => jsObject.tfliteModel;

  @override
  String get updateTime => jsObject.updateTime;

  @override
  String get validationError => jsObject.validationError;

  @override
  Future<void> waitForUnlocked([int maxTimeSeconds]) {
    if (maxTimeSeconds == null) {
      return handleThenable(jsObject.waitForUnlocked());
    } else {
      return handleThenable(jsObject.waitForUnlocked(maxTimeSeconds));
    }
  }

  ///A Firebase ML Model output object
  static Model getInstance(ml_interop.ModelJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= Model._fromJsObject(jsObject);
  }
}

///A Firebase ML Model input object
class ModelOptions extends JsObjectWrapper<ml_interop.ModelOptionsJsImpl>
    implements _ModelOptions {
  static final _expando = Expando<ModelOptions>();

  ///A Firebase ML Model input object
  factory ModelOptions({
    String displayName,
    List<String> tags,
    ml_interop.TFLiteModel tfliteModel,
  }) =>
      ModelOptions._fromJsObject(ml_interop.ModelOptionsJsImpl(
        displayName: displayName,
        tfliteModel: tfliteModel,
        tags: tags,
      ));

  ModelOptions._fromJsObject(ml_interop.ModelOptionsJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  String get displayName => jsObject.displayName;

  @override
  set displayName(String v) => jsObject.displayName = v;

  @override
  List<String> get tags => List.from(jsObject.tags);

  @override
  set tags(List<String> v) => jsObject.tags = jsify(v);

  @override
  ml_interop.TFLiteModel get tfliteModel => jsObject.tfliteModel;

  @override
  set tfliteModel(ml_interop.TFLiteModel v) => jsObject.tfliteModel = v;

  ///A Firebase ML Model input object
  static ModelOptions getInstance(ml_interop.ModelOptionsJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= ModelOptions._fromJsObject(jsObject);
  }
}

abstract class _ListModelsResult {
  ///A list of models in your project.
  List<Model> get models;

  ///A token you can use to retrieve the next page of results. If null, the
  ///current page is the final page.
  String get pageToken;
}

abstract class _MachineLearning {
  /// The admin.app.App associated with the current MachineLearning service
  /// instance.
  App get app;

  /// Creates a model in Firebase ML.
  Future<void> createModel(ModelOptions model);

  /// Deletes a model from Firebase ML.
  Future<void> deleteModel(String modelId);

  ///Gets a model from Firebase ML.
  Future<Model> getModel(String modelId);

  ///Lists models from Firebase ML.
  Future<ListModelsResult> listModels(ml_interop.ListModelsOptions options);

  ///Publishes a model in Firebase ML.
  Future<Model> publishModel(String modelId);

  ///Unpublishes a model in Firebase ML.
  Future<Model> unpublishModel(String modelId);

  ///Updates a model in Firebase ML.
  Future<Model> updateModel(String modelId, ModelOptions model);
}

abstract class _Model {
  ///The timestamp of the model's creation.
  String get createTime;

  ///The model's name. This is the name you use from your app to load the model.
  String get displayName;

  ///The ETag identifier of the current version of the model. This value
  ///changes whenever you update any of the model's properties.
  String get etag;

  ///True if the model is locked by a server-side operation. You can't make
  ///changes to a locked model. See waitForUnlocked().
  bool get locked;

  ///The hash of the model's tflite file. This value changes only when you
  ///upload a new TensorFlow Lite model.
  String get modelHash;

  ///The ID of the model.
  String get modelId;

  ///True if the model is published.
  bool get published;

  ///The model's tags.
  List<String> get tags;

  ///Metadata about the model's TensorFlow Lite model file.
  ml_interop.TFLiteModel get tfliteModel;

  ///The timestamp of the model's most recent update.
  String get updateTime;

  ///Error message when model validation fails.
  String get validationError;

  ///Wait for the model to be unlocked.
  Future<void> waitForUnlocked([int maxTimeSeconds]);
}

abstract class _ModelOptions {
  ///A name for the model. This is the name you use from your app to load the
  ///model.
  String displayName;

  ///Tags for easier model management.
  List<String> tags;

  ///An object containing the URI of the model in Cloud Storage.
  ml_interop.TFLiteModel tfliteModel;
}
