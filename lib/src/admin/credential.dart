part of '../../admin.dart';

/// Interface that provides Google OAuth2 access tokens used to authenticate
/// with Firebase services.
///
/// In most cases, you will not need to implement this yourself and can instead
/// use the default implementations provided by admin.credential.
class Credential extends JsObjectWrapper<credential_interop.CredentialJsImpl> {
  static final _expando = Expando<Credential>();

  Credential._fromJsObject(credential_interop.CredentialJsImpl jsObject)
      : super.fromJsObject(jsObject);

  /// Returns a Google OAuth2 access token object used to authenticate with
  /// Firebase services.
  ///
  /// This object contains the following properties:
  /// - `access_token`: The actual Google OAuth2 access token.
  /// - `expires_in`: The number of seconds from when the token was issued
  /// that it expires.
  ///
  /// Returns Future<GoogleOAuthAccessToken>
  /// A Google OAuth2 access token object.
  Future<admin_interop.GoogleOAuthAccessToken> getAccessToken() =>
      handleThenable(jsObject.getAccessToken());

  /// Interface that provides Google OAuth2 access tokens used to authenticate
  /// with Firebase services.
  ///
  /// In most cases, you will not need to implement this yourself and can
  /// instead use the default implementations provided by admin.credential.
  static Credential getInstance(credential_interop.CredentialJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= Credential._fromJsObject(jsObject);
  }
}

/// Instance of admin.credential
class CredentialBuilder {
  /// Instance of admin.credential
  const CredentialBuilder();

  /// Returns a credential created from the Google Application Default
  /// Credentials that grants admin access to Firebase services. This credential
  /// can be used in the call to admin.initializeApp().
  ///
  /// Google Application Default Credentials are available on any Google
  /// infrastructure, such as Google App Engine and Google Compute Engine.
  Credential applicationDefault([HttpAgent httpAgent]) {
    if (httpAgent != null) {
      return Credential._fromJsObject(
          credential_interop.applicationDefault(httpAgent));
    }
    return Credential._fromJsObject(credential_interop.applicationDefault());
  }

  /// Returns a credential created from the provided service account that grants
  /// admin access to Firebase services. This credential can be used in the call
  /// to admin.initializeApp().
  ///
  /// Valid values are `ServiceAccount` or `String`
  Credential cert(dynamic serviceAccountPathOrObject, [HttpAgent httpAgent]) {
    if (httpAgent != null) {
      return Credential._fromJsObject(credential_interop.cert(
          jsify(serviceAccountPathOrObject), httpAgent));
    }
    return Credential._fromJsObject(
        credential_interop.cert(jsify(serviceAccountPathOrObject)));
  }

  /// Returns a credential created from the provided refresh token that grants
  /// admin access to Firebase services. This credential can be used in the call
  /// to admin.initializeApp().
  ///
  /// Valid values are `Map<String,dynamic>` or `String`
  Credential refreshToken(
    dynamic refreshTokenPathOrObject, [
    HttpAgent httpAgent,
  ]) {
    if (httpAgent != null) {
      return Credential._fromJsObject(credential_interop.refreshToken(
          jsify(refreshTokenPathOrObject), httpAgent));
    }
    return Credential._fromJsObject(
        credential_interop.refreshToken(jsify(refreshTokenPathOrObject)));
  }
}
