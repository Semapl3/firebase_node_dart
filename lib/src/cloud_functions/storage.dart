import 'package:js/js.dart';

import '../utils/js.dart';
import '../utils/utils.dart';
import 'functions.dart';
import 'interop/storage_interop.dart' as interop;

export 'interop/storage_interop.dart' show CustomerEncryption, ObjectOwner;

class BucketBuilder extends JsObjectWrapper<interop.BucketBuilderJsImpl> {
  static final _expando = Expando<BucketBuilder>();

  static BucketBuilder getInstance(interop.BucketBuilderJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= BucketBuilder._fromJsObject(jsObject);
  }

  BucketBuilder._fromJsObject(interop.BucketBuilderJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///Event handler which fires every time a Google Cloud Storage change occurs.
  ObjectBuilder object() => ObjectBuilder.getInstance(jsObject.object());
}

class ObjectBuilder extends JsObjectWrapper<interop.ObjectBuilderJsImpl> {
  static final _expando = Expando<ObjectBuilder>();

  static ObjectBuilder getInstance(interop.ObjectBuilderJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= ObjectBuilder._fromJsObject(jsObject);
  }

  ObjectBuilder._fromJsObject(interop.ObjectBuilderJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///Event handler sent only when a bucket has enabled object versioning. This
  ///event indicates that the live version of an object has become an archived
  ///version, either because it was archived or because it was overwritten by
  ///the upload of an object of the same name.
  dynamic onArchieve(CloudFunction<ObjectMetadata> handler) {
    return jsObject.onArchieve(allowInterop((dataJs, contextJs) {
      final data = ObjectMetadata._fromJsObject(dataJs);
      final context = EventContext.fromJsObject(contextJs);
      return cloudFunction(data, context, handler);
    }));
  }

  ///Event handler which fires every time a Google Cloud Storage deletion
  ///occurs.
  ///
  ///Sent when an object has been permanently deleted. This includes objects
  ///that are overwritten or are deleted as part of the bucket's lifecycle
  ///configuration. For buckets with object versioning enabled, this is not
  ///sent when an object is archived, even if archival occurs via the storage.
  ///objects.delete method.
  dynamic onDelete(CloudFunction<ObjectMetadata> handler) {
    return jsObject.onDelete(allowInterop((dataJs, contextJs) {
      final data = ObjectMetadata._fromJsObject(dataJs);
      final context = EventContext.fromJsObject(contextJs);
      return cloudFunction(data, context, handler);
    }));
  }

  ///Event handler which fires every time a Google Cloud Storage object
  ///creation occurs.
  ///
  ///Sent when a new object (or a new generation of an existing object) is
  ///successfully created in the bucket. This includes copying or rewriting an
  ///existing object. A failed upload does not trigger this event.
  dynamic onFinalize(CloudFunction<ObjectMetadata> handler) {
    return jsObject.onFinalize(allowInterop((dataJs, contextJs) {
      final data = ObjectMetadata._fromJsObject(dataJs);
      final context = EventContext.fromJsObject(contextJs);
      return cloudFunction(data, context, handler);
    }));
  }

  ///Event handler which fires every time the metadata of an existing object
  ///changes.
  dynamic onMetadataUpdate(CloudFunction<ObjectMetadata> handler) {
    return jsObject.onMetadataUpdate(allowInterop((dataJs, contextJs) {
      final data = ObjectMetadata._fromJsObject(dataJs);
      final context = EventContext.fromJsObject(contextJs);
      return cloudFunction(data, context, handler);
    }));
  }
}

class ObjectMetadata extends JsObjectWrapper<interop.ObjectMetadataJsImpl> {
  static final _expando = Expando<ObjectMetadata>();

  ObjectMetadata._fromJsObject(interop.ObjectMetadataJsImpl jsObject)
      : super.fromJsObject(jsObject);

  Map<String, dynamic> get acl => dartify(jsObject.acl);

  ///Storage bucket that contains the object.
  String get bucket => jsObject.bucket;

  ///The value of the Cache-Control header, used to determine whether Internet
  ///caches are allowed to cache public data for an object.
  String get cacheControl => jsObject.cacheControl;

  ///Specifies the number of originally uploaded objects from which a composite
  ///object was created.
  String get componentCount => jsObject.componentCount;

  ///The value of the Content-Disposition header, used to specify presentation
  ///information about the data being transmitted.
  String get contentDisposition => jsObject.contentDisposition;

  ///Content-Encoding to indicate that an object is compressed (for example,
  ///with gzip compression) while maintaining its Content-Type.
  String get contentEncoding => jsObject.contentEncoding;

  ///ISO 639-1 language code of the content.
  String get contentLanguage => jsObject.contentLanguage;

  ///The object's content type, also known as the MIME type.
  String get contentType => jsObject.contentType;

  ///The object's CRC32C hash. All Google Cloud Storage objects have a CRC32C
  ///hash or MD5 hash.
  String get crc32c => jsObject.crc32c;

  ///Customer-supplied encryption key.
  ///
  ///This object contains the following properties:
  ///
  ///`encryptionAlgorithm (string|null)`: The encryption algorithm that was
  ///used. Always contains the value AES256.
  ///
  ///`keySha256 (string|mull)`: An RFC 4648 base64-encoded string of the
  ///SHA256 hash of your encryption key. You can use this SHA256 hash to
  ///uniquely identify the AES-256 encryption key required to decrypt the
  ///object, which you must store securely.
  interop.CustomerEncryption get customerEncryption =>
      jsObject.customerEncryption;

  String get etag => jsObject.etag;

  ///Generation version number that changes each time the object is overwritten.
  String get generation => jsObject.generation;

  ///The ID of the object, including the bucket name, object name, and
  ///generation number.
  String get id => jsObject.id;

  ///The kind of the object, which is always storage#object.
  String get kind => jsObject.kind;

  ///MD5 hash for the object. All Google Cloud Storage objects have a CRC32C
  ///hash or MD5 hash.
  String get md5Hash => jsObject.md5Hash;

  ///Media download link.
  String get mediaLink => jsObject.mediaLink;

  ///User-provided metadata.
  String get metadata => dartify(jsObject.metadata);

  ///Meta-generation version number that changes each time the object's
  ///metadata is updated.
  String get metageneration => jsObject.metageneration;

  ///The object's name.
  String get name => jsObject.name;

  interop.ObjectOwner get owner => jsObject.owner;

  ///Link to access the object, assuming you have sufficient permissions.
  String get selfLink => jsObject.selfLink;

  ///The value of the Content-Length header, used to determine the length of
  ///the object data in bytes.
  String get size => jsObject.size;

  ///Storage class of the object.
  String get storageClass => jsObject.storageClass;

  ///The creation time of the object in RFC 3339 format.
  String get timeCreated => jsObject.timeCreated;

  ///The deletion time of the object in RFC 3339 format. Returned only if this
  ///version of the object has been deleted.
  String get timeDeleted => jsObject.timeDeleted;

  String get timeStorageClassUpdated => jsObject.timeStorageClassUpdated;

  ///The modification time of the object metadata in RFC 3339 format.
  String get updated => jsObject.updated;

  static ObjectMetadata getInstance(interop.ObjectMetadataJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= ObjectMetadata._fromJsObject(jsObject);
  }
}

class StorageFunctionsBuilder
    extends JsObjectWrapper<interop.StorageFunctionsBuilderJsImpl> {
  StorageFunctionsBuilder.fromJsObject(
      interop.StorageFunctionsBuilderJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///The optional bucket function allows you to choose which buckets' events to
  ///handle. This step can be bypassed by calling object() directly, which will
  ///use the default Cloud Storage for Firebase bucket.
  BucketBuilder bucket(String bucket) =>
      BucketBuilder.getInstance(jsObject.bucket(bucket));

  ///Handle events related to Cloud Storage objects.
  ObjectBuilder object() => ObjectBuilder.getInstance(jsObject.object());
}
