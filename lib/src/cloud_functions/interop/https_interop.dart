// ignore_for_file: public_member_api_docs
@JS()
library firebase_node.functions.interop.https;

import 'package:js/js.dart';
import 'package:node_interop/http.dart' as node;

import 'functions_interop.dart';

@JS()
@anonymous
abstract class CallableContextJsImpl {
  ///The result of decoding and verifying a Firebase Auth ID token.
  external AuthContextJsImpl get auth;

  ///An unverified token for a Firebase Instance ID.
  external String get instanceIdToken;

  ///The raw request handled by the callable.
  external node.IncomingMessage get rawRequest;
}

@JS()
@anonymous
abstract class HttpsFunctionsBuilderJsImpl {
  ///Declares a callable method for clients to call using a Firebase SDK.
  external dynamic onCall(Function(dynamic, CallableContextJsImpl) handler);

  ///Handle HTTP requests.
  external dynamic onRequest(node.HttpRequestListener handler);
}

@JS()
@anonymous
abstract class HttpsErrorJsImpl {
  external factory HttpsErrorJsImpl({
    String code,
    dynamic details,
    String message,
    String name,
    String stack,
  });

  ///A standard error code that will be returned to the client. This also
  ///determines the HTTP status code of the response, as defined in code.proto.
  external String get code;

  ///Extra data to be converted to JSON and included in the error response.
  external dynamic get details;

  external String get message;

  external String get name;

  external String get stack;
}
