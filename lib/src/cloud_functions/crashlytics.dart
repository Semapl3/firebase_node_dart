import 'package:js/js.dart';

import '../utils/js.dart';
import 'functions.dart';
import 'interop/crashlytics_interop.dart' as interop;

export 'interop/crashlytics_interop.dart'
    show CrashlyticAppInfo, Issue, VelocityAlert;

///Construct crashlytics functions
class CrashlyticsFunctionsBuilder
    extends JsObjectWrapper<interop.CrashlyticFunctionsBuilderJsImpl> {
  ///Construct crashlytics functions
  CrashlyticsFunctionsBuilder.fromJsObject(
      interop.CrashlyticFunctionsBuilderJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///Handle events related to Crashlytics issues. An issue in Crashlytics is an
  ///aggregation of crashes which have a shared root cause.
  IssueBuilder issue() => IssueBuilder.getInstance(jsObject.issue());
}

///Construct crashlytics issue functions
class IssueBuilder extends JsObjectWrapper<interop.IssueBuilderJsImpl> {
  static final _expando = Expando<IssueBuilder>();

  ///Construct crashlytics issue functions
  static IssueBuilder getInstance(interop.IssueBuilderJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= IssueBuilder._fromJsObject(jsObject);
  }

  IssueBuilder._fromJsObject(interop.IssueBuilderJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///Event handler that fires every time a new issue occurs in a project.
  dynamic onNew(CloudFunction<interop.Issue> handler) {
    return jsObject.onNew(allowInterop((issue, contextJs) {
      final context = EventContext.fromJsObject(contextJs);
      return cloudFunction(issue, context, handler);
    }));
  }

  ///Event handler that fires every time a regressed issue reoccurs in a
  ///project.
  dynamic onRegressed(CloudFunction<interop.Issue> handler) {
    return jsObject.onRegressed(allowInterop((issue, contextJs) {
      final context = EventContext.fromJsObject(contextJs);
      return cloudFunction(issue, context, handler);
    }));
  }

  ///Event handler that fires every time a velocity alert occurs in a project.
  dynamic onVelocityAlert(CloudFunction<interop.Issue> handler) {
    return jsObject.onVelocityAlert(allowInterop((issue, contextJs) {
      final context = EventContext.fromJsObject(contextJs);
      return cloudFunction(issue, context, handler);
    }));
  }
}
